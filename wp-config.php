<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'dgr' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'admin' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', 'admin' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'ckJ$d+S*cne20m,tD}!m_ (vyp4`UseM$^!cd<2Ei]o7X{P%wW76uEhlO`vj_?fW' );
define( 'SECURE_AUTH_KEY',  '3g)[]0-<y=c4*k9vAo0<TO.$fA=,qtEOl^J@eA?,Nl+>W`W=<yp@7 b&N 4Xt m4' );
define( 'LOGGED_IN_KEY',    'w<Cf#d,36TH!I6eQ=!W,}w6w~@U`YN-d[UqaWtEi%l7&K<z(:%!P>nc3*ULN@f%A' );
define( 'NONCE_KEY',        '.*}4B@k6Fl#po1#j/tRF/)^+p!6C*}Km7}Ct48Q97$?@0BMm @YZ|!K77R:|,*lX' );
define( 'AUTH_SALT',        'frq;83gvSdhK?5zVaoc.!*X`5OSMx.E%_dWRJP&[{A,kYKLXmd] Bvt L2HS6rO!' );
define( 'SECURE_AUTH_SALT', 'd_0Mjsc<<NHnKV1-csAaxsiYU/Vtx*G1cNi>j$}df^{=MHjZXYj9b?5^Jp99cUzx' );
define( 'LOGGED_IN_SALT',   'W@Cv$aJpHc-$F|#4<L!#3Sxw0M+.4k4^]FB-vZ~>L8U25RWl:.(Q1${ZtX2ng{#C' );
define( 'NONCE_SALT',       '/]o}6C`|2LYn#DM(1z,l<Y{8{cfF=jwukRwVDda+J{;E/o(ZW;!tBRuBtPCr^#nc' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'wp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
