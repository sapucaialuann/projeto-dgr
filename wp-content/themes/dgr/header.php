<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css?family=Aldrich|PT+Serif&display=swap" rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"> -->
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css"> -->
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>DGR-Contadora</title>
    <?php wp_head(); ?>
</head>

<body>
    <header>
        <section class="container-plus">
            <div class="logo">
                <a href="#"><img src="" class="img-header"></a>
            </div>
            <div class="menu">
                <ul class="menu-list">
                    <li>
                        <a href="#">Serviços</a>
                    </li>
                    <li>
                        <a href="#">Notícias</a>
                    </li>
                    <li>
                        <a href="#">Contato</a>
                    </li>
                </ul>
                <div class="menu-tel">
                    <p id="first-tel">1321311</p>
                    <p id="second-tel">2131321</p>
                </div>
            </div>
        </section>
    </header>