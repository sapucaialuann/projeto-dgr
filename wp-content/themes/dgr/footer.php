<!-- FALTA INSERIR OS SCRIPTS NA PAGINA! -->

<?php 
//NÃO TEM $SOBRE NESSE FOOTER!!!!!!! 
$contato = get_page_by_title('Contato');
?>

    <footer>
        <div class="footer">
            <div class="container">
                <div class="content">
                    <div class="map footer-item">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3675.5492449107546!2d-43.12320178540075!3d-22.89310384313752!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x9983c38ccdfc39%3A0x3b33119087859018!2sR.+Maestro+Fel%C3%ADcio+Tol%C3%AAdo%2C+519+-+Centro%2C+Niter%C3%B3i+-+RJ%2C+24030-103!5e0!3m2!1spt-BR!2sbr!4v1564848702227!5m2!1spt-BR!2sbr" width="450" height="250" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                    <div class="contact footer-item">
                        <p><?php the_field('endereco', $contato); ?></p>
                        <p><?php the_field('email', $contato); ?></p>
                        <p><?php the_field('telefone', $contato); ?></p>
                    </div>
                    <div class="creator-logo footer-item">
                        <p>Desenvolvido por:</p>
                        <img src="assets/img/logo-injunior.png" alt="logo da empresa injunior">
                    </div>
                </div>
            </div>
        </div>
        <div id="copyright">
            <p>Copyright &copy; 2015 - DGR Contabilidade todos os direitos reservados.</p>
        </div>
    </footer>
    <?php wp_footer(); ?>
</body>
<script src=""></script>
<script src=""></script>
</html>